package chapter34;

import javax.swing.JOptionPane;

/**
 * This is my boolean and if else program exercise
 * 
 * @author Julia Chirite
 *
 */

public class chapter34 {

	public static void main(String[] args) {
		String num1 = JOptionPane.showInputDialog("Enter number of people");
		double numPeople = Integer.parseInt(num1);

		double groupSize;
		if (numPeople > 10) {
			groupSize = numPeople / 2;
			System.out.println("For " + numPeople + " people the group size is " + groupSize);
		} else if ((3 <= numPeople) && (numPeople <= 10)) {
			groupSize = numPeople / 3;
			System.out.println("For " + numPeople + " people the group size is " + groupSize);
		} else {
			System.out.println("The number of people has to be at least 3.");
		}

		String num2 = JOptionPane.showInputDialog("Enter number of players");
		double numPlayer = Integer.parseInt(num2);

		double teamSize = numPlayer / 11;
		if ((11 <= numPlayer) && (numPlayer <= 55)) {
			System.out.println("For " + numPlayer + " players the team size is " + teamSize);
		} else {
			System.out.println("The team size is 1");
		}
	}
}