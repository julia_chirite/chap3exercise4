
public class Calculator {

	//public static double getAverage(double first, double second, double third) {
	//return (first + second + third)/ 3.0;
	//}

	public static double getAverage(double [] num) {
		double sum = 0.0;
		//loop through array
		for(int i = 0; i<num.length; i++) {
			sum+=num[i];
		}
		return(sum/num.length);
	}
	
	public static double getSum(double [] num) {
		double sum = 0.0;
		for(int i = 0;i<num.length;i++) {
			sum+=num[i];
		}
		return(sum);
	}
	
	public static double getProduct(double [] num) {
		double product = 1;
		for(int i = 0; i<num.length; i++) {
			product*=num[i];
		}
		return(product);
	}



}
