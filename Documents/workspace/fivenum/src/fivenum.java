
import java.util.Scanner;

public class fivenum {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);

		System.out.println("Enter five whole numbers separated by one space");
		int num1, num2, num3, num4, num5;
		num1 = keyboard.nextInt();
		num2 = keyboard.nextInt();
		num3 = keyboard.nextInt();
		num4 = keyboard.nextInt();
		num5 = keyboard.nextInt();

		int sum = (num1 + num2 + num3 + num4 + num5);
		System.out.println("The sum of the numbers you entered is " + sum);
		int average = (sum / 5);
		System.out.println("The average of the numbers you entered is " + average);

	}

}
